import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
import RootReducer from "./reducers/RootReducer";
import { persistStore } from "redux-persist";

import logger from "redux-logger";

const initialState = {};
const middlewares = [thunk];
let devtools = (x) => x;

if (
  process.env.NODE_ENV !== "production" &&
  // process.browser &&
  window.__REDUX_DEVTOOLS_EXTENSION__
) {
  middlewares.push(logger);

  devtools = window.__REDUX_DEVTOOLS_EXTENSION__();
}

export const Store = createStore(
  RootReducer,
  initialState,
  compose(applyMiddleware(...middlewares), devtools)
);

export const persistor = persistStore(Store);
