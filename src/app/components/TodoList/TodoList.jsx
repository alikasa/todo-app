import React, { useEffect } from "react";
import MaterialTable from "material-table";
import { Icons } from "../constants/Icons";
import { connect } from "react-redux";
import {
  getTodosListAction,
  createTodoAction,
  updateTodoAction,
  deleteTodoAction,
} from "../../redux/actions/TodoActions";

function TodoList({ currentUser, getTodosList, todos, createTodo }) {
  const { useState } = React;
  const [columns, setColumns] = useState([
    {
      title: "Name",
      field: "username",
      editable: "onAdd",
      validate: (rowData) => rowData.username?.length >= 1,
    },
    {
      title: "E-mail",
      field: "email",
      editable: "onAdd",
      validate: (rowData) => {
        const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        return rowData?.email
          ? regex.test(rowData.email)
            ? true
            : "E-mail is invalid"
          : false;
      },
    },
    {
      title: "Text",
      field: "text",
      validate: (rowData) => rowData.text?.length >= 1,
    },
    {
      title: "Status",
      field: "status",
      lookup: {
        0: "task not completed",
        1: "task not completed, edited by admin",
        10: "task completed",
        11: "task edited by admin and completed",
      },
    },
  ]);

  // const [data, setData] = useState([
  //   {
  //     username: "Zerya Betül",
  //     email: "Baran@gmail.com",
  //     text: "cook for dinner",
  //     status: 0,
  //   },
  // ]);
  useEffect(() => {
    getTodosList();
  }, []);

  let editable = {
    onRowAdd: (newData) =>
      new Promise((resolve, reject) => {
        // setTimeout(() => {
        //   // setData([...data, newData]);

        // }, 1000);

        var form = new FormData();
        form.append("username", "Example");
        form.append("email", "example@example.com");
        form.append("text", "Some text");
        console.log(newData, form);
        createTodo(form);

        resolve();
      }),
  };

  if (!!currentUser) {
    editable = {
      ...editable,
      onRowUpdate: (newData, oldData) =>
        new Promise((resolve, reject) => {
          setTimeout(() => {
            // const dataUpdate = [...data];
            // const index = oldData.tableData.id;
            // dataUpdate[index] = newData;
            // setData([...dataUpdate]);

            resolve();
          }, 1000);
        }),
      // onRowDelete: (oldData) =>
      //   new Promise((resolve, reject) => {
      //     setTimeout(() => {
      //       const dataDelete = [...data];
      //       const index = oldData.tableData.id;
      //       dataDelete.splice(index, 1);
      //       setData([...dataDelete]);

      //       resolve();
      //     }, 1000);
      //   }),
    };
  }
  return (
    <MaterialTable
      title="Todo List"
      columns={columns}
      data={todos.length ? todos : []}
      icons={Icons}
      editable={editable}
      options={{
        pageSize: 3,
        pageSizeOptions: [3, 5, 10],
      }}
    />
  );
}

const mapStateToProps = (state) => ({
  currentUser: state.user.currentUser,
  todos: state.todo.todos,
});

const mapDispatchToProps = (dispatch) => ({
  getTodosList: () => dispatch(getTodosListAction()),
  createTodo: (todo) => dispatch(createTodoAction(todo)),
});
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
