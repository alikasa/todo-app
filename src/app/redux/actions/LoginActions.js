import jwtAuthService from "../../services/jwtAuthService";
import { setUserData } from "./UserActions";

import { setError } from "./setError";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_LOADING = "LOGIN_LOADING";

export function loginWithUsernameAndPasswordAction({ username, password }) {
  return (dispatch) => {
    dispatch({
      type: LOGIN_LOADING,
    });

    jwtAuthService
      .loginWithUsernameAndPassword(username, password)
      .then((data) => {
        dispatch(setUserData(data.user));
        setTimeout(() => {
          history.push({
            pathname: "/",
          });
          // window.location.reload();
        });
        return dispatch({
          type: LOGIN_SUCCESS,
        });
      })
      .catch((error) => {
        setError(error, dispatch);

        return dispatch({
          type: LOGIN_ERROR,
          payload: error,
        });
      });
  };
}
