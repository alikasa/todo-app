import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";

import LoginReducer from "./LoginReducer";
import TodoReducer from "./TodoReducer";
import SpinReducer from "./SpinReducer";
import UserReducer from "./UserReducer";

import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["user"],
};

const RootReducer = combineReducers({
  login: LoginReducer,
  todo: TodoReducer,
  user: UserReducer,
  spin: SpinReducer,
});

export default persistReducer(persistConfig, RootReducer);
