import instance from "../../services/axios";
import { setError, setSuccess } from "./setError";
// import { setSpin } from "./SpinAction";
export const SET_TODO_DATA = "TODO_SET_DATA";
export const REMOVE_TODO_DATA = "TODO_REMOVE_DATA";
export const TODO_LOGGED_OUT = "TODO_LOGGED_OUT";

export const GET_TODO_LIST = "GET_TODO_LIST";
export const CREATE_TODO = "CREATE_TODO";
export const UPDATE_TODO = "UPDATE_TODO";
export const DELETE_TODO = "DELETE_TODO";

export function setTodoData(todo) {
  return (dispatch) => {
    dispatch({
      type: SET_TODO_DATA,
      data: todo,
    });
  };
}

export function getTodosListAction(query) {
  return async (dispatch) => {
    try {
      // dispatch(setSpin(true));
      const todoList = await instance.get("/v2?developer=Alibek");
      console.log(todoList.data.message.tasks);
      dispatch({
        type: GET_TODO_LIST,
        payload: todoList.data.message.tasks,
      });
      // dispatch(setSpin(false));
    } catch (error) {
      // dispatch(
      //   createNotification({
      //     message: error?.response?.data?.message
      //       ? error?.response?.data?.message
      //       : error?.message,
      //     state: "error",
      //   })
      // );
    }
  };
}

export function createTodoAction(todo) {
  return async (dispatch) => {
    try {
      // dispatch(setSpin(true));
      console.log(todo);

      const newTodo = await instance.post("/v2/create?developer=Alibek", todo);
      dispatch(getTodosListAction());

      setSuccess(newTodo.data, dispatch);
    } catch (error) {
      setError(error, dispatch);
    }
  };
}

export function updateTodoAction(todo) {
  return async (dispatch) => {
    try {
      // dispatch(setSpin(true));

      const updateTodo = await instance.put("/todo/" + todo._id, todo);
      // dispatch(getTodosList(todoFilter(todo.role)));
      setSuccess(updateTodo.data, dispatch);
    } catch (error) {
      setError(error, dispatch);
    }
  };
}

export function deleteTodoAction(todo) {
  return async (dispatch) => {
    try {
      // dispatch(setSpin(true));

      const todos = await instance.delete("/todo/" + todo._id);
      // dispatch(getTodosList(todoFilter(todo.role)));
      setSuccess(todos.data, dispatch);
    } catch (error) {
      setError(error, dispatch);
    }
  };
}
