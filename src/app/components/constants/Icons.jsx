import React from "react";
import { Icon } from "@material-ui/core";
import {
  RiCloseLine,
  RiPencilLine,
  RiDeleteBinLine,
  RiCheckLine,
  RiDownload2Line,
  RiAddLine,
  RiSearchLine,
  RiFilter3Line,
} from "react-icons/ri";
import {
  FaIndent,
  FaAngleDoubleLeft,
  FaAngleDoubleRight,
  FaChevronRight,
  FaChevronLeft,
} from "react-icons/fa";
export const Icons = {
  Delete: () => (
    <Icon color="error">
      <RiDeleteBinLine />
    </Icon>
  ),
  Add: () => (
    <Icon>
      <RiAddLine />
    </Icon>
  ),
  Check: () => (
    <Icon>
      <RiCheckLine />
    </Icon>
  ),
  Clear: () => <RiCloseLine />,
  DetailPanel: () => (
    <Icon>
      <FaIndent />
    </Icon>
  ),
  Edit: () => <RiPencilLine style={{ color: "gray", opacity: "0.8" }} />,
  Export: () => (
    <Icon>
      <RiDownload2Line />
    </Icon>
  ),
  Filter: () => (
    <Icon>
      <RiFilter3Line />
    </Icon>
  ),
  FirstPage: () => (
    <Icon>
      <FaAngleDoubleLeft />
    </Icon>
  ),
  // SortArrow: () => (
  //   <FaLongArrowAltUp style={{ color: "gray", opacity: "0.5" }} />
  // ),
  LastPage: () => (
    <Icon>
      <FaAngleDoubleRight />
    </Icon>
  ),
  NextPage: () => (
    <Icon>
      <FaChevronRight />
    </Icon>
  ),
  PreviousPage: () => (
    <Icon>
      <FaChevronLeft />
    </Icon>
  ),
  ResetSearch: () => (
    <Icon>
      <RiCloseLine />
    </Icon>
  ),
  Search: () => (
    <Icon>
      <RiSearchLine />
    </Icon>
  ),
  ThirdStateCheck: () => (
    <Icon>
      <RiCheckLine />
    </Icon>
  ),
};
